# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
 - `Modulizer` class splits a big ltlcross task into smaller ones, execute the small tasks in parallel, and merge the intermediate results into one final `.csv` file.

### Removed
 - `modulizer` that was not running itself in parallel

## [0.3] - 2019-12-27
### Added
 - `ResultsAnalyzer` is a new class that contains the functionality to analyze & visualize results of ltlcross runs
 - `cumulative` now can take `tool_subset` as an arguments, which restricts the computation to the given subset of tools. Only formulas with timeouts within the given tool subset are removed from the sum. Run the following command to obtain results where all formulas with some timeout are removed.
```python
analyzer.cumulative().loc[tool_subset]
```
 - `ResultsAnalyzer` can create scatter plots using bokeh (requires [bokeh](https://bokeh.org/) and [colorcet](https://colorcet.holoviz.org/)).
 Showing the plots in [JupyterLab](https://jupyterlab.readthedocs.io/en/stable/) requires [jupyter_bokeh](https://github.com/bokeh/jupyter_bokeh) extension.
 - `modulizer` class is a wrapper that splits a big ltlcross task into smaller ones and executes the small ones one by one, merging the partial files into final ones when finished. It is suited for parallel execution.

### Changed
 - `cummulative` renamed to `cumulative`
 - default value of `timeout` changed to 2 minutes
 - `LtlcrossRunner` only runs ltlcross and can't be used to visualize the results — that's the job of a new class: `ResultsAnalyzer`.

## [0.2.0] - 2019-12-06
### Added
 - ltlcross tasks can be split, run in parallel, and the results merged back

[Unreleased]: https://gitlab.com/fandikb/ltlcross-runner/compare/v0.3...HEAD
[0.3]: https://gitlab.com/fandikb/ltlcross-runner/compare/v0.2.0...v0.3
[0.2.0]: https://gitlab.com/fandikb/ltlcross-runner/tags/v0.2.0
